package co.edureka.project;

import java.util.Locale;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import co.edureka.dao.UsersDAO;
import co.edureka.model.Users;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private ApplicationContext context;		
	private UsersDAO dao;
	
	
	public HomeController() {
		this.context = new ClassPathXmlApplicationContext("SpringConfig.xml");		
		this.dao = context.getBean("usersDAO", UsersDAO.class);
	}
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		return "index";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Locale locale, Model model) {
		return "login";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Locale locale, Model model) {
		return "register";
	}
	
	@RequestMapping(value = "/map", method = RequestMethod.GET)
	public String map(Locale locale, Model model) {
		return "map";
	}

	@RequestMapping(value = "/registerProcess", method = RequestMethod.POST)
	public String registerProcess(@RequestParam("exampleInputEmail1") String email, 
								  @RequestParam("exampleInputPassword1") String password,
								  @RequestParam("exampleInputName1") String name,
								  Locale locale, Model model) {
		dao.insertUser(new Users(email,password,name));
		return "login";
	}
	
	@RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
	public String registerProcess(@RequestParam("exampleInputEmail1") String email, 
								  @RequestParam("exampleInputPassword1") String password,
								  Locale locale, Model model) {
		Users user = dao.findUser(email, password);
		System.out.println(email+" "+password);
		if(user!=null)
		return "landingFromLogin";
		
		return "login";
	}
	

	@RequestMapping(value = "/chat", method = RequestMethod.GET)
	public String chat(Locale locale, Model model) {
			
		return "chat";
	}
	
	
	
}
