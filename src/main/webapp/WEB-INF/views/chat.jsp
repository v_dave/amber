<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title></title>
<link rel="stylesheet"
	href="https://bootswatch.com/4/yeti/bootstrap.min.css">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
</head>
<body>
	<div class="row" style="margin: 20px;">
		<h2>Shawn</h2>
	</div>
	<div class="row" style="margin: 20px;">
		<div class="col-md-12">
			<blockquote class="blockquote text-left">
				<p class="mb-0">Hi!!. Whats up!</p>
				<footer class="blockquote-footer">
					I am a freshman at NEU CS and was <br>looking for someone to
					study with for tommorow's test.<br> <cite title="Source Title">8:45a.m.</cite>
				</footer>
			</blockquote>
		</div>
	</div>
	<br>
	<div class="row" style="margin: 20px;">
		<div class="col-md-12">
			<blockquote class="blockquote text-right">
				<p class="mb-0">Sure!</p>
				<footer class="blockquote-footer">
					Lets meet in the library.<br> <cite title="Source Title">9:30a.m.</cite>
				</footer>
			</blockquote>
		</div>
	</div>

	<div class="row" style="position: fixed; bottom: 1px; left: 10px">
		<div class="col-md-7">
			<div class="form-group">
				<input class="form-control form-control-lg" type="text"
					placeholder="Enter test here" id="inputLarge" style="width: 80vw">
			</div>
		</div>
		<div class="col-md-5"></div>
			<input type="submit" value="Send" class="btn btn-primary" style="margin-left: 20px">
		
	</div>

</body>
</html>