<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://bootswatch.com/4/yeti/bootstrap.min.css">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<title>Buddy Finder</title>
</head>
<body>
<div class="row" style="height: 110vw">
<div class="col-md-6 jumbotron" style="">
<h4 class="display-4">Bored, Sad or just having a vibe of doing something crazy?</h4><br>
<h4 class="display-4">We have got everything for you!!</h4>
</div>
<div class="col-md-1"></div>
<div class="col-md-4">
<div class="list-group" style="margin-top: 18vw">
  <h2 class="list-group-item list-group-item-action active" style="padding-left: 5vw; padding-right: 5vw">Amber</h2>
  <a style="padding-left: 5vw; padding-right: 5vw" href="<%=request.getContextPath()%>/login" class="list-group-item list-group-item-action" style="padding: 1vw">Login</a>
  <a style="padding-left: 5vw; padding-right: 5vw" href="<%=request.getContextPath()%>/register" class="list-group-item list-group-item-action" style="padding: 1vw">Register</a>
</div>
</div>
</div>
</div>
</body>
</html>