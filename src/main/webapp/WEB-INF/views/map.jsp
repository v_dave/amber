<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Find People around</title>
<link rel="stylesheet"
	href="https://bootswatch.com/4/yeti/bootstrap.min.css">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<style>
/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
#map {
	height: 100%;
}
/* Optional: Makes the sample page fill the window. */
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}
</style>
</head>
<body style="background-color: black">
	<div class="modal" tabindex="-1" role="dialog" id="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Shawn Walker	
					</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close" name="close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>I am looking for someone to study for tommorow's algo test with me.</p>
				</div>
				
				<div class="modal-footer">
					<a href="${pageContext.request.contextPath}/chat" class="btn btn-primary">Connect & hang out!</a>
				</div>
			</div>
		</div>
	</div>
	<script>
	var modal = document.getElementById('myModal');
	var span = document.getElementsByClassName("close")[0];
	span.onclick = function() {
	    modal.style.display = "none";
	}
	</script>

	<center>
		<h2 style="color: white; padding-top: 2vw">People around you to
			hangout with!</h2>
	</center>
	<container>
	<div id="map" style="margin: 100px"></div>
	</container>
	<script>
		function initMap() {
			var locations = [ [ 'Deesha', 42.343243, -71.096042, 4 ],
					[ 'Dhaval', 42.33439, -71.100892, 2 ],
					[ 'Vaibhav', 42.334428, -71.100214, 3 ],
					[ 'Shawn', 42.343884, -71.087897, 1 ] ];

			// Styles a map in night mode.
			var map = new google.maps.Map(document.getElementById('map'), {
				center : {
					lat : 42.340176,
					lng : -71.08928
				},
				zoom : 12,
				styles : [ {
					"featureType" : "road",
					"elementType" : "labels",
					"stylers" : [ {
						"visibility" : "on"
					} ]
				}, {
					"featureType" : "poi",
					"stylers" : [ {
						"visibility" : "off"
					} ]
				}, {
					"featureType" : "administrative",
					"stylers" : [ {
						"visibility" : "off"
					} ]
				}, {
					"featureType" : "road",
					"elementType" : "geometry.fill",
					"stylers" : [ {
						"color" : "#000000"
					}, {
						"weight" : 1
					} ]
				}, {
					"featureType" : "road",
					"elementType" : "geometry.stroke",
					"stylers" : [ {
						"color" : "#000000"
					}, {
						"weight" : 0.8
					} ]
				}, {
					"featureType" : "landscape",
					"stylers" : [ {
						"color" : "#ffffff"
					} ]
				}, {
					"featureType" : "water",
					"stylers" : [ {
						"visibility" : "off"
					} ]
				}, {
					"featureType" : "transit",
					"stylers" : [ {
						"visibility" : "off"
					} ]
				}, {
					"elementType" : "labels",
					"stylers" : [ {
						"visibility" : "off"
					} ]
				}, {
					"elementType" : "labels.text",
					"stylers" : [ {
						"visibility" : "on"
					} ]
				}, {
					"elementType" : "labels.text.stroke",
					"stylers" : [ {
						"color" : "#ffffff"
					} ]
				}, {
					"elementType" : "labels.text.fill",
					"stylers" : [ {
						"color" : "#000000"
					} ]
				}, {
					"elementType" : "labels.icon",
					"stylers" : [ {
						"visibility" : "on"
					} ]
				} ]
			});

			var infowindow = new google.maps.InfoWindow();

			var marker, i;

			for (i = 0; i < locations.length; i++) {
				marker = new google.maps.Marker({
					position : new google.maps.LatLng(locations[i][1],
							locations[i][2]),
					map : map
				});

				google.maps.event.addListener(marker, 'click', (function(
						marker, i) {

					return function() {
						var modal = document.getElementById('myModal');
						modal.style.display = "block";
						infowindow.setContent(locations[i][0]);
						infowindow.open(map, marker);
					}
				})(marker, i));
			}
		}
	</script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxSmurcyjcF67JT574kdSQWJZBRYapWv4&callback=initMap"
		async defer></script>
</body>
</html>