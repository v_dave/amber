<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://bootswatch.com/4/yeti/bootstrap.min.css">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
</head>
<body>
<div class="row">
<div class="col-md-4">
</div>
<div class="col-md-4">
<form method="post" action="${pageContext.request.contextPath}/registerProcess">
  <fieldset>
    <center><h2><strong>Register</strong></h2></center>
    <div class="form-group">
     <h2><label for="exampleInputName1"><strong>Name</strong></label></h2>
      <input type="text" class="form-control" name="exampleInputName1" id="exampleInputName1" aria-describedby="emailHelp" placeholder="Enter Name">
    </div>
    <div class="form-group">
     <h2><label for="exampleInputEmail1"><strong>Email address</strong></label></h2>
      <input type="email" class="form-control" id="exampleInputEmail1" name="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
      <h2><label for="exampleInputPassword1"><strong>Password</strong></label></h2>
      <input type="password" class="form-control" id="exampleInputPassword1" name="exampleInputPassword1" placeholder="Password">
    </div>
        <div class="form-group">
      <h2><label for="exampleInputPassword1"><strong>Enter Password Again</strong></label></h2>
      <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Repeat Password">
    </div>
    <div class="birth-day">
    <h2><strong> Date of Birth</strong></h2>
    <div class="row">
    <div class="col-md-3">
      <label for="day1">Day</label>
      <select class="form-control" id="day1">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
        <option>10</option>        
        <option>11</option>
        <option>12</option>
        <option>13</option>
        <option>14</option>
        <option>15</option>        
        <option>16</option>
        <option>17</option>
        <option>18</option>
        <option>19</option>
        <option>20</option>        
        <option>21</option>
        <option>22</option>
        <option>23</option>
        <option>24</option>
        <option>25</option>        
        <option>26</option>
        <option>27</option>
        <option>28</option>
        <option>29</option>
        <option>30</option>
        <option>31</option>
      </select>
    </div>
    <div class="col-md-4">
      <label for="month1">Month</label>
      <select class="form-control" id="month1">
        <option>January</option>
        <option>February</option>
        <option>March</option>
        <option>April</option>
        <option>May</option>
        <option>June</option>
        <option>July</option>
        <option>August</option>
        <option>September</option>
        <option>October</option>
        <option>November</option>
        <option>December</option>
        </select>
        </div>
      <div class="col-md-3">
      <label for="year1">Year</label>
      <select class="form-control" id="year1">
        <option>1965</option>
        <option>1966</option>
        <option>1977</option>
        <option>1988</option>
        <option>1999</option>
        <option>2009</option>
        </select>
        </div>
    </div>
  </div>
    <div class="form-group">
      <h2><label for="exampleTextarea"><strong>Say something cool about yourself</strong></label></h2>
      <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
    </div>
    <div class="form-group">
      <h2><label for="exampleInputFile"><strong>Upload your most pretty picture!</strong></label></h2>
      <center><input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp"></center>
      <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
    </div>
    <fieldset class="form-group">
      <legend>Gender</legend>
      <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1" checked>
          <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1" checked>
          Male
        </label>
      </div>
      <div class="form-check">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2">
          Female
        </label>
      </div>
      <div class="form-check">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2">
          Other
        </label>
      </div>
    </fieldset>
    
   <fieldset class="form-group">
      <legend>Would you like details of event near you?</legend>
      <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="optionsRadios1" id="optionsRadios1" value="option1" checked="">
          Yes
        </label>
      </div>
      <div class="form-check">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="optionsRadios1" id="optionsRadios2" value="option2">
          No
        </label>
      </div>
      
    </fieldset>
    <input type="submit" class="btn btn-primary" value="Submit"/>
  </fieldset>
</form>	
</div>
<div class="col-md-4"></div>
</div>
</body>
</html>