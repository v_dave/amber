<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://bootswatch.com/4/yeti/bootstrap.min.css">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<title>BuddyFinder</title>
</head>
<body>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=1095071190545329&autoLogAppEvents=1';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="row" style="margin-top: 5vw;margin-left: 3vw">

			<div class="col-md-12">
				<form method="post" action="${pageContext.request.contextPath}/loginProcess" style="border-color: black;">
					<fieldset>
						<center>
							<h1>
								<strong>Login</strong>
							</h1>
						</center>
						<div class="form-group">
							<h2>
								<label for="exampleInputEmail1"><strong>Email
										address</strong></label>
							</h2>
							<input type="email" class="form-control" id="exampleInputEmail1" name="exampleInputEmail1"
								aria-describedby="emailHelp" placeholder="Enter email">
							<small id="emailHelp" class="form-text text-muted">We'll
								never share your email with anyone else.</small>
						</div>
						<div class="form-group">
							<h2>
								<label for="exampleInputPassword1"><strong>Password</strong></label>
							</h2>
							<input type="password" class="form-control"
								id="exampleInputPassword1" name="exampleInputPassword1" placeholder="Password">
							<center>
								<input type="submit" class="btn btn-primary"
									style="margin: 1vw" value="Login"/>
							</center>
							<div class="fb-login-button" style="margin-left: 2vw" data-max-rows="1" data-size="large"
					data-button-type="continue_with" data-show-faces="true"
					data-auto-logout-link="false" data-use-continue-as="false"></div>
						</div>
					</fieldset>
				
				</form>
				
				
			</div>
		</div>
	</div>
</body>