<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://bootswatch.com/4/yeti/bootstrap.min.css">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<title>Main Page</title>
</head>
<body>
	<script>
		function doThis() {
			theDiv = document.getElementById("div2");
			document.getElementById('div2').style.visibility = 'hidden';
			theDiv = document.getElementById("div3");
			document.getElementById('div3').style.visibility = 'hidden';
			theDiv = document.getElementById("div4");
			document.getElementById('div4').style.visibility = 'hidden';
			theDiv = document.getElementById("div5");
			document.getElementById('div5').style.visibility = 'hidden';
			theDiv = document.getElementById("div6");
			document.getElementById('div6').style.visibility = 'hidden';
			theDiv = document.getElementById("div7");
			document.getElementById('div7').style.visibility = 'hidden';
			theDiv = document.getElementById("div8");
			document.getElementById('div8').style.visibility = 'hidden';
			theDiv = document.getElementById("div10");
			document.getElementById('div10').style.visibility = 'hidden';
			theDiv = document.getElementById("div11");
			document.getElementById('div11').style.visibility = 'hidden';
			theDiv = document.getElementById("div12");
			document.getElementById('div12').style.visibility = 'hidden';
			theDiv = document.getElementById("div13");
			document.getElementById('div13').style.visibility = 'hidden';
			console.log("this");

		}
	</script>
	<div class="row"
		style="margin-left: 5vw; margin-top: 20vw; position: fixed; z-index: 1">
		<div class="col-md-4"></div>
		<div class="col-md-8">
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
				<form class="form-inline my-2 my-lg-0">
					<h2 style="color: white;">
						<strong>What do you want to do today?</strong>
					</h2>
					<input class="form-control mr-sm-2" type="text"
						placeholder="Search">
					<button class="btn btn-secondary my-2 my-sm-0" onclick="doThis()"
						type="button">Search</button>
				</form>
			</nav>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3" id="div1">
					<div class="card border-primary mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Study</a></div>
						<div class="card-body">
							<p class="card-text">Not sure what to do? Get a drink. Talk
								to someone. Socialize.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-3" id="div2">
					<div class="card border-secondary mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Sport</a></div>
						<div class="card-body">
							<p class="card-text">Play a Sport. Go for a run. Get Fit.</p>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-3" id="div3">
					<div class="card border-success mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Eating</a></div>
						<div class="card-body">
							<p class="card-text">Grab a bite. Get a drink. Dinner? Lunch?
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-3" id="div4">
					<div class="card border-danger mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Studying</a></div>
						<div class="card-body">
							<p class="card-text">Need help with your next assignment? Or
								a good study partner? You're at the right place.</p>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-3" id="div5">
					<div class="card border-warning mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Concerts</a></div>
						<div class="card-body">
							<p class="card-text">Need to attend the concert in town and
								don't have a buddy? FInd one partner here.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-3" id="div6">
					<div class="card border-info mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Movies</a></div>
						<div class="card-body">
							<p class="card-text">Scared to watch the new horror flick. No
								worries, find a buddy to watch it with you.</p>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-3" id="div7">
					<div class="card border-light mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Cab Sharing</a></div>
						<div class="card-body">
							<p class="card-text">Bored to ride alone? Want someone to
								travel with you? CLick here.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-3" id="div8">
					<div class="card border-dark mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Dancing</a></div>
						<div class="card-body">
							<p class="card-text">No one to dance? Need a Salsa Partner
								for the enxt dance class. Relax, you'll find one here.</p>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-3" id="div9">
					<div class="card border-primary mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Socializing</a></div>
						<div class="card-body">
							<p class="card-text">Not sure what to do? Get a drink. Talk
								to someone. Socialize.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-3" id="div10">
					<div class="card border-secondary mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Sport</a></div>
						<div class="card-body">
							<p class="card-text">Play a Sport. Go for a run. Get Fit.</p>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-3" id="div11">
					<div class="card border-success mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Eating</a></div>
						<div class="card-body">
							<p class="card-text">Grab a bite. Get a drink. Dinner? Lunch?
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-3" id="div12">
					<div class="card border-danger mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Studying</a></div>
						<div class="card-body">
							<p class="card-text">Need help with your next assignment? Or
								a good study partner? You're at the right place.</p>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-3" id="div13">
					<div class="card border-warning mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Concerts</a></div>
						<div class="card-body">
							<p class="card-text">Need to attend the concert in town and
								don't have a buddy? Find one partner here.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-3" id="div14">
					<div class="card border-info mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Movies</a></div>
						<div class="card-body">
							<p class="card-text">Scared to watch the new horror flick. No
								worries, find a buddy to watch it with you.</p>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-3" id="div15">
					<div class="card border-light mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Cab Sharing</a></div>
						<div class="card-body">
							<p class="card-text">Bored to ride alone? Want someone to
								travel with you? CLick here.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-3" id="div16">
					<div class="card border-dark mb-3" style="max-width: 20rem;">
						<div class="card-header"><a href="${pageContext.request.contextPath}/map">Dancing</a></div>
						<div class="card-body">

							<p class="card-text">No one to dance? Need a Salsa Partner
								for the enxt dance class. Relax, you'll find one here.</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>s
</body>
</html>